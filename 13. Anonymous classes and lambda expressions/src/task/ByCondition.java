package task;

import java.util.Objects;

@FunctionalInterface
public interface ByCondition {
    boolean isOk (int number);

    default ByCondition and (ByCondition other) {
        Objects.requireNonNull(other);
        return number -> isOk(number) && other.isOk(number);

    }
}
