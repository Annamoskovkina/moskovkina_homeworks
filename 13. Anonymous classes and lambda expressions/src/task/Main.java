package task;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        int[] oldResult = Sequence.filter(new int[]{12, 16, -10, 782, 987, 101, 501}, val -> val % 2 ==0);
        System.out.println("Эти элемента массива - четные :  " + Arrays.toString(oldResult));

        int[] result = Sequence.filter(new int[]{12, 16, -10, 782, 987, 101, 501}, number -> sumOfDigits(number) % 2 ==0);
        System.out.println("Сумма цифр этих элемента массива четная :  "+ Arrays.toString(result));

        int[] newResult = Sequence.filter(new int[]{12, 16, 10, 782, 987, 101, 34}, Main::isSumOfDigitsEven);
        System.out.println(Arrays.toString(newResult));


    }

    public static int sumOfDigits(int number) {
        return (number == 0) ? 0 : (number % 10) + sumOfDigits(number / 10);
    }

    public static boolean isSumOfDigitsEven(int number) {
        int sum = 0;
        while (number != 0) {
            int recent = number % 10;
            sum = sum + recent;
            number = number / 10;
        }
        return sum % 2 == 0;
    };



}





