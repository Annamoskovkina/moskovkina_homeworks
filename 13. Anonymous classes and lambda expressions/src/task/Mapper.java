package task;

import java.util.Objects;

@FunctionalInterface
public interface Mapper <T,R> {

    R apply(T t);

   default <V> Mapper<V,R> compose (Mapper<? super V, ? extends T> before) {
       Objects.requireNonNull(before);
       return (V v) -> apply(before.apply(v));
   }
}
