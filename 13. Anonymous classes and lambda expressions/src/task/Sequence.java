package task;

import java.util.Arrays;

public class Sequence {
    public static int[] filter(int[] array, ByCondition newCondition) {
        int[] result = new int[array.length];
        int count = 0;
        for (int c : array) {
            if (newCondition.isOk(c)) {

                result[count++] = c;
            }
        }

        return Arrays.copyOfRange(result, 0, count);
    }
}
