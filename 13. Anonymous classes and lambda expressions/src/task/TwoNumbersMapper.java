package task;

public interface TwoNumbersMapper {
    int map(int a, int b);
}
