package App;

import model.User;
import model.UsersRepository;
import model.UsersRepositoryFileImpl;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("Users_db.txt");
        List<User> users = usersRepository.findAll();
        System.out.println("Метод найти всех:");

        for (User user : users) {
            System.out.println(user.getId() + " " + user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
        System.out.println();

        System.out.println("Следующий метод- поиск по возрасту:");

        List<User> ageUsers = usersRepository.findByAge(27);

        for (User user : ageUsers) {
            System.out.println(user.getId() + " " + user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
        System.out.println();

        List<User> workerUsers = usersRepository.findByIsWorkerIsTrue();

        System.out.println("Следующий метод- кто работает:");
        for (User user : workerUsers) {
            System.out.println(user.getId() + " " + user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
        System.out.println();

        User idUsers = usersRepository.findById(3);

        System.out.println("Следующий метод - поиск по Id:"  + idUsers);


        idUsers.setName("Марсель");
        idUsers.setAge(27);
        usersRepository.update(idUsers);

        List<User> updatedUsers = usersRepository.findAll();

        System.out.println();
        System.out.println("Обновленный файл:");

        for (User user : updatedUsers) {
            System.out.println(user);
        }
    }

}
