package model;

import App.NotFoundException;

import java.io.*;
import java.util.*;


public class UsersRepositoryFileImpl implements UsersRepository {

    private String fileName;
    private final Map<Integer, User> users = new HashMap<>();






    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }


    //поиск по Id
    @Override
    public User findById(int id) throws NotFoundException {
        List <User> users = new ArrayList<>();
        for (User user : findAll()) {
            if (user.getId() ==id) {
                return user;
            }
        }

        return null;
    }

    //обновление сущностей
    @Override
        public void update(User user) {
        List<User> users = findAll();
        for (User u : users) {
            if (u.getId() == user.getId()) {
                users.remove(u);
                break;
            }
        }
        users.add(user);
        Writer writer = null;
        BufferedWriter bufferedWriter = null;
        try {
            writer = new FileWriter(fileName, false);
            bufferedWriter = new BufferedWriter(writer);
            for (User u : users) {
                bufferedWriter.write(u.getId() + "|" + u.getName() + "|" + u.getAge() + "|" + u.isWorker());
                bufferedWriter.newLine();
                bufferedWriter.flush();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignore) {
                }
            }
        }
    }


    // поиск всех
    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        // объявили переменные для доступа
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            // создали читалку на основе файла
            reader = new FileReader(fileName);
            // создали буферизированную читалку
            bufferedReader = new BufferedReader(reader);
            // прочитали строку
            String line = bufferedReader.readLine();
            // пока к нам не пришла "нулевая строка"
            while (line != null) {
                // разбиваем ее по |
                String[] parts = line.split("\\|");
                // берем id
                int id = Integer.parseInt(parts[0]);
                // берем имя
                String name = parts[1];
                // берем возраст
                int age = Integer.parseInt(parts[2]);
                // берем статус о работе
                boolean isWorker = Boolean.parseBoolean(parts[3]);
                // создаем нового человека
                User newUser = new User(id, name, age, isWorker);
                // добавляем его в список
                users.add(newUser);
                // считываем новую строку
                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            // этот блок выполнится точно
            if (reader != null) {
                try {
                    // пытаемся закрыть ресурсы
                    reader.close();
                } catch (IOException ignore) {}
            }
            if (bufferedReader != null) {
                try {
                    // пытаемся закрыть ресурсы
                    bufferedReader.close();
                } catch (IOException ignore) {}
            }
        }

        return users;
    }


    @Override
    public List<User> findByAge(int age) {
        // TODO: реализовать++
        List <User> users = new ArrayList<>();
        for (User user : findAll()) {
            if (user.getAge() ==age) {
                users.add(user);
            }
        }

        return users;
    }

    @Override
    public List<User> findByIsWorkerIsTrue() {
        // TODO: реализовать++
        List<User> users = new ArrayList<>();
        for(User user: findAll()) {
            if (user.isWorker()) {
                users.add(user);

            }
        }
        return users;
    }
}
