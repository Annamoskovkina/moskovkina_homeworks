import java.util.Scanner;

public class homework05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number:");
        int a = scanner.nextInt();
        int mindigit = 9;
        int lastDigit = 0;
        while (a != -1) {

            while (a >= 1) {
                lastDigit = a % 10;
                a = a / 10;
                System.out.println("number    " + lastDigit);
            }

            if (lastDigit < mindigit) {
                mindigit = lastDigit  ;

            }
            a = scanner.nextInt();
        }



        System.out.println("minimum of digits = " + mindigit);
    }
}
