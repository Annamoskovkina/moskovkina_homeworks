
import java.util.Arrays;
import java.util.Scanner;

class tasktoarray {

    /*
    функция поиска индекса введенного числа.
     */
    public static int searchIndexOfArray(int[] nums) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число для поиска: ");
        int inputNumber = scanner.nextInt();
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == inputNumber) {

                return i;

            }
        }
        return -1;
    }


    /*
    функция перемещения значимых элементов влево
        */

    public static void moveElementsToLeft(int[] nums) {
        int position = 0;
        for (int i= 0; i < nums.length; i++) {
            if (nums[i] != 0) {
                nums [position++] = nums [i];}

            }
        for (; position < nums.length; position++) {
                nums [position] = 0;
        }

        }


    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int[] nums = new int[12];

        for (int i = 0; i < nums.length; i++) {
            nums[i] = s.nextInt();
        }

        System.out.println("Введенный массив  " + Arrays.toString(nums));
        System.out.println("индекс искомого числа :  " + searchIndexOfArray(nums));
        moveElementsToLeft(nums);
        System.out.println("Измененный массив:  " + Arrays.toString(nums));


    }
}
