import java.sql.SQLOutput;
import java.util.Scanner;

class Homework07 {

    public static void main(String[] args) {
        int[] array = new int[201];
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите числа");
        int a = scanner.nextInt();
        while (a != -1) {
            array[100 + a]++;
            a = scanner.nextInt();
        }
        int repeat = 2147483647;
        
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < repeat && array[i] != 0) {
                repeat = array[i];
                index = i;
            }
        }
        System.out.println("Элемент массива, c наименьшим повторением: " + (index - 100));
    }
}

