public class Ellipse extends Figure{
    protected int r;
    public int r2;

    public Ellipse (int x, int y, int r, int r2) {
        super(x,y);
        this.r = r;
        this.r2 = r2;
    }
    public double getPerimeter() {
        return Math. PI * (r + r2);
    }

}
