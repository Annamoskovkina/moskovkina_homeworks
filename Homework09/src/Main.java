public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(10, 20, 0, 0);
        Square square = new Square(0,0,8);
        Ellipse ellipse = new Ellipse(0, 0, 2, 5);
        Circle circle = new Circle(0, 0, 5);
        Figure figure = new Figure(0, 0);


        Figure[] figures = new Figure[5];
        figures[0] = figure;
        figures[1] = rectangle;
        figures[2] = square;
        figures[3] = ellipse;
        figures[4] = circle;

        for (int i = 0; i < figures.length; i++) {
            double perimeter = figures[i].getPerimeter();
            System.out.println(perimeter);

        }
    }
}
