public abstract class Figure implements Movable {
    protected int x;
    protected int y;

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
    public abstract double getPerimeter();

    @Override
    public void moveTo(int dx, int dy) {
        x += dx;
        y += dy;
        System.out.println("Центр фигуры переместился на следующие координаты:   " + "x: " + dx +"; " + "y: " + dy +".");
    }
}
