public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(10, 20, 0, 0);
        rectangle.getPerimeter();
        System.out.println("Периметр прямоугольника: " + rectangle.getPerimeter());

        Square square = new Square(0,0,8);
        square.getPerimeter();
        System.out.println("Периметр квадрата: " + square.getPerimeter());

        Ellipse ellipse = new Ellipse(0, 0, 2, 5);
        ellipse.getPerimeter();
        System.out.println("Длина окружности овала: " + ellipse.getPerimeter());

        Circle circle = new Circle(0, 0, 5);
        circle.getPerimeter();
        System.out.println("Длина окружности круга: " + circle.getPerimeter());

        circle.moveTo(3, 20);
        square.moveTo(3,20);
    }
}
