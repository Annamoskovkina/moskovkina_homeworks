public interface Movable {
    void moveTo(int dx, int dy);

}
