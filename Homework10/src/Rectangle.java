public class Rectangle extends Figure {
    protected int a;
    protected int b;

    public Rectangle(int a, int b, int x, int y) {
        super(x,y);
        this.a = a;
        this.b = b;
    }

    public double getPerimeter() {
        return a * 2 + b * 2;
    }
}
