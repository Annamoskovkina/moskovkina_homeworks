public class Logger {
    private static final Logger instance;

    static {
        instance = new Logger();
    }

    private Logger() {
        this.message = new String[MAX_MESSAGE_CUNT];
    }

    public static Logger getInstance() {
        return instance;
    }

    private static final int MAX_MESSAGE_CUNT = 10;
    private String[] message;
    private int count;

    public void log() {
        for (int i = 0; i < MAX_MESSAGE_CUNT; i++) {
            System.out.println(message[i]);
        }
    }

    public void add(String message){
        if (count < MAX_MESSAGE_CUNT){
            this.message[count]=message;
            count++;
        }else{
            System.err.println("Переполнение сообщений");
        }
       }

}
