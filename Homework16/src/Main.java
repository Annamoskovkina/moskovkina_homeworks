import java.sql.SQLOutput;

public class Main {

    public static void main(String[] args) {

        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(45);
        numbers.add(78);
        numbers.add(10);
        numbers.add(17);
        numbers.add(89);
        numbers.add(16);

        System.out.println("size = " + numbers.size());
        for (int i = 0; i < numbers.size(); i++) {
            System.out.print(numbers.get(i) + " ");
        }

        System.out.println();
        numbers.removeAt(4);
        System.out.println("size = " + numbers.size());
        for (int i = 0; i < numbers.size(); i++) {
            System.out.print(numbers.get(i) + " ");

        }
        System.out.println();
        LinkedList<Integer> list = new LinkedList<>();
        list.add(34);
        list.add(120);
        list.add(-10);
        list.add(11);
        list.add(50);
        list.add(100);
        list.add(88);

        list.addToBegin(77);
        list.addToBegin(88);
        list.addToBegin(99);

        System.out.println(list.get(5));
        System.out.println(list.get(2));

    }
}

