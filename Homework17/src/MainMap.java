import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class MainMap {
    public static void main(String[] args) {
        //На вход подается строка с текстом. Необходимо посчитать, сколько встречается раз каждое слово в этой строке.
        //
        //Вывести:
        //Слово - количество раз
        //
        //Использовать Map, string.split(" ") - для деления текста по словам. Слово - символы, ограниченные пробелами справа и слева.
        Map<String, Integer> map = new HashMap<>();

        System.out.println("Введите текст: ");
        String strLine = new Scanner(System.in).nextLine();
        String[] words = strLine.split(" ");

        for (String word : words) {
            if(map.keySet().contains(word)) {
                map.put(word, map.get(word) + 1);
            } else {
                map.put(word, 1);
            }
        }
        Set<Map.Entry<String, Integer>> entries = map.entrySet();

        for (Map.Entry<String, Integer> entry : entries) {
            System.out.println("Слово: '" + entry.getKey() + "'. Встречается : " + entry.getValue() + " раз");
        }
    }
}
