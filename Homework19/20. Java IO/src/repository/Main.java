package repository;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        List<User> users = usersRepository.findAll();
        System.out.println("Метод найти всех:");

        for (User user : users) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }

        System.out.println(" Следующий метод- поиск по возрасту:");

        List<User> ageUsers = usersRepository.findByAge(27);

        for (User user : ageUsers) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }

        List<User> workerUsers = usersRepository.findByIsWorkerIsTrue();

        System.out.println(" Следующий метод- кто работает:");
        for (User user : workerUsers) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
    }
}
