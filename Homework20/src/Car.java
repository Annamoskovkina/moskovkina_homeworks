import java.util.Objects;

public class Car {
    private String NumberOfCar;
    private Long MileageOfCar;
    private Long PriceOfCar;
    private String ModelOfCar;
    private String ColorOfCar;

    public Car(String numberOfCar,  String modelOfCar, String colorOfCar,Long mileageOfCar, Long priceOfCar) {
        NumberOfCar = numberOfCar;
        MileageOfCar = mileageOfCar;
        PriceOfCar = priceOfCar;
        ModelOfCar = modelOfCar;
        ColorOfCar = colorOfCar;
    }

    public String getNumberOfCar() {
        return NumberOfCar;
    }

    public void setNumberOfCar(String numberOfCar) {
        NumberOfCar = numberOfCar;
    }

    public Long getMileageOfCar() {
        return MileageOfCar;
    }

    public void setMileageOfCar(Long mileageOfCar) {
        MileageOfCar = mileageOfCar;
    }

    public Long getPriceOfCar() {
        return PriceOfCar;
    }

    public void setPriceOfCar(Long priceOfCar) {
        PriceOfCar = priceOfCar;
    }

    public String getModelOfCar() {
        return ModelOfCar;
    }

    public void setModelOfCar(String modelOfCar) {
        ModelOfCar = modelOfCar;
    }

    public String getColorOfCar() {
        return ColorOfCar;
    }

    public void setColorOfCar(String colorOfCar) {
        ColorOfCar = colorOfCar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(NumberOfCar, car.NumberOfCar) && Objects.equals(MileageOfCar, car.MileageOfCar) && Objects.equals(PriceOfCar, car.PriceOfCar) && Objects.equals(ModelOfCar, car.ModelOfCar) && Objects.equals(ColorOfCar, car.ColorOfCar);
    }

    @Override
    public int hashCode() {
        return Objects.hash(NumberOfCar, MileageOfCar, PriceOfCar, ModelOfCar, ColorOfCar);
    }

    @Override
    public String toString() {
        return "number: " + NumberOfCar
                + " /model: '" + ModelOfCar + "'"
                + " color:  '" + ColorOfCar + "'"
                + "    mileage: " + MileageOfCar
                + "    price: " + PriceOfCar + "\n \n";
    }
}
