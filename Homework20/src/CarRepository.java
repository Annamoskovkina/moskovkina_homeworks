import java.util.List;

public interface CarRepository {
    // Метод предоставляет список объектов типа Car (список автомобилей).

    // @return - список типа Car.

    List<Car> findAll();

    //Метод производит поиск объектов типа Car по двум условиям (цвет автомобиля или пробег).

    //@return - список типа String (номера автомобилей).

    List<String> findBlackCarsAndZeroMileage();


    // Метод производит поиск уникальных объектов типа Car по условию (ценовой диапазон).
    // @return - число (количество уникальных автомобилей).

    Long findAmountModelsOfCarsWithPriceBetween700And800();

    // Метод производит поиск объекта типа Car по условию (минимальная стоимость автомобиля).
    // @return - строка (цвет автомобиля).

    String findColorOfCarWithMinPrice();

    //Метод производит поиск объектов типа Car по условию (модель автомобиля).
    // @return - число (средняя стоимость автомобилей определенной модели).

    Double findAveragePriceOfCamry();
}
