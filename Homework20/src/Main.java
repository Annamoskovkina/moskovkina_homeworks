public class Main {
    //Подготовить файл с записями, имеющими следующую структуру:
    //[НОМЕР_АВТОМОБИЛЯ][МОДЕЛЬ][ЦВЕТ][ПРОБЕГ][СТОИМОСТЬ]
    //
    //o001aa111|Camry|Black|133|82000
    //o002aa111|Camry|Green|133|0
    //o001aa111|Camry|Black|133|82000
    //
    //Используя Java Stream API, вывести:
    //
    //Номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map
    //Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter
    //* Вывести цвет автомобиля с минимальной стоимостью. // min + map
    //* Среднюю стоимость Camry *
    //
    //https://habr.com/ru/company/luxoft/blog/270383/
    //
    //Достаточно сделать один из вариантов.
    public static void main(String[] args) {
        CarRepository carRepository = new MethodsForCars();
        System.out.println("\n"+"Контрольный перечень автомобилей:");
        System.out.println(carRepository.findAll());
        System.out.println("\n"+"Номера черных автомобилей и/или с нулевым пробегом:");
        System.out.println(carRepository.findBlackCarsAndZeroMileage());

        System.out.println("\n"+"Количество уникальных моделей в ценовом диапазоне от 700000 до 800000:");
        System.out.println(carRepository.findAmountModelsOfCarsWithPriceBetween700And800());

        System.out.println("\n"+"Автомобиль с минимальной ценой имеет следующий цвет:");
        System.out.println(carRepository.findColorOfCarWithMinPrice());

        System.out.println("\n"+"Средняя стоимость автомобилей Camry");
        System.out.println(carRepository.findAveragePriceOfCamry());
    }

}
