import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {
    private int id;
    private String description;
    private Double price;
    private int amount;

    public Product(String description, Double price, int amount) {

        this.description = description;
        this.price = price;
        this.amount = amount;
    }
}


