import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;


import javax.sql.DataSource;
import java.util.List;

public class ProductRepositoryJdbcTemplateImpl implements ProductRepository {

    //language = SQL
    private static final String SQL_INSERT = "insert into product(description, price, amount) values(?,?,?)";

    //language = SQL
    private static final String SQL_SELECT_ALL = "select * from product order by id";
    private JdbcTemplate jdbcTemplate;

    //language = SQL
    private static final String SQL_SELECT_BY_PRICE = "select * from product  where price =?";

    //language = SQL
    private static final String SQL_SELECT_BY_ORDERS_COUNT = "select * from product where (select count(*) from order where product_id = product.id) = ?";



    public ProductRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> {


        int id = row.getInt("id");
        String description = row.getString("description");
        Double price = row.getDouble("price");
        int amount = row.getInt("amount");

        return new Product(id, description, price, amount);

    };

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getDescription(), product.getPrice(), product.getAmount());


    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query(SQL_SELECT_BY_PRICE, ps -> ps.setDouble(1,price), productRowMapper);

        }

    @Override
    public List<Product> findAllByOrdersCount(int ordersCount) {
        String SQL_FIND_ALL_BY_ORDERS_COUNT =
                "SELECT p.id, p.description, p.price, p.amount FROM product p " +
                        "INNER JOIN \"order\" o ON o.product_id = p.id " +
                        "GROUP BY (p.id, p.description, p.price, p.amount) " +
                        "HAVING count(o.product_id) = ?";

        return jdbcTemplate.query(SQL_FIND_ALL_BY_ORDERS_COUNT, productRowMapper, ordersCount);
    }
}

