import java.util.Scanner;

class Program08 {

    public static void sortWeight(Human[] array) {
        int i = 0;
        while (i < array.length) {
            for (int j = i; j < array.length; j++) {
                if (array[j].getWeight() < array[i].getWeight()) {
                    Human temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
            i++;
        }
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        Human[] humans = new Human[10];

        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human();
            System.out.println("Введите имя: ");
            humans[i].setName(scanner.nextLine());
            System.out.println("Введите вес: ");
            humans[i].setWeight(Integer.parseInt(scanner.nextLine()));
        }

        sortWeight(humans);

        for (Human human : humans) {
            System.out.println(human.getName() + " " + human.getWeight() + " кг.");
        }
    }
}

