import java.util.Scanner;

class Human {

    Scanner scanner = new Scanner(System.in);

    public String setName;
    private String name;

    private float weight;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getWeight() {
        return this.weight;
    }

    public void setWeight(int weight) {
        if (weight < 0 || weight > 250) {
            System.out.println("Уточните вес");
            Integer.parseInt(scanner.nextLine());
            return;
        }
        this.weight = weight;
    }

}
