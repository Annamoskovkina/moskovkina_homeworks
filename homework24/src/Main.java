import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

public class Main {
    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/postgres",
                "postgres", "Rubin1987");
        ProductRepository productRepository = new ProductRepositoryJdbcTemplateImpl(dataSource);
        System.out.println(productRepository.findAll());
        System.out.println(productRepository.findAllByPrice(50.0));
        System.out.println(productRepository.findAllByOrdersCount(1));

        Product product = Product.builder()
                .description("Potato")
                .price(52.0)
                .amount(50)
                .build();
    }
}
