CREATE TABLE product
(
    id          SERIAL PRIMARY KEY,
    description CHARACTER VARYING(100) NOT NULL,
    price       NUMERIC(9,2) NOT NULL,
    amount INTEGER CHECK (amount >=0) NOT NULL
);

CREATE TABLE customer
(
    id          SERIAL PRIMARY KEY,
    name VARCHAR(30) NOT NULL,
    lastname VARCHAR(50) NOT NULL

);

CREATE TABLE "order" (
    product_id INT NOT NULL,
    customer_id INT NOT NULL,
    order_date DATE NOT NULL,
    product_amount INT CHECK ( product_amount >= 0 ) NOT NULL,
    FOREIGN KEY (product_id) REFERENCES Product(id) ON DELETE CASCADE,
    FOREIGN KEY (customer_id) REFERENCES Customer(id) ON DELETE CASCADE
);
INSERT INTO product (description, price, amount)
VALUES
('milk', 60, 3),
('phone', 40210, 1),
('ball', 1200, 2),
('car', 700000, 3),
('beer', 70, 2),
('dress', 3500, 8);

INSERT INTO customer (name, lastname)
VALUES
    ('Anna', 'Moskovkina'),
    ('Oleg', 'Kuzmin'),
    ('Alex', 'Kirillov'),
    ('Omsk', 'Citiesov'),
    ('Kate', 'Greatfuliva');

INSERT INTO "order" (product_id, customer_id, order_date, product_amount)
VALUES
    (1,1, '04-12-2021', 3),
    (1,2, '03-12-2021', 2),
    (2,2, '01-11-2021', 3),
    (3,4, '10-10-2021', 1),
    (4,3, '27-11-2021', 1);

--ищем покупателя с именем Anna
SELECT * FROM customer
WHERE name LIKE '%Anna%';

--ищем покупки за октябрь и ноябрь
SELECT * FROM "order"
WHERE order_date
BETWEEN '01-10-2021' AND '30-11-2021';

--ищем покупателей без покупок
SELECT c.id, c.name, c.lastname FROM customer c
    LEFT JOIN "order" o ON o.customer_id = c.id
WHERE o.customer_id IS NULL;

--ищем покупки Олега
SELECT order_date, product_amount, customer_id FROM "order"
 LEFT JOIN customer ON customer_id = id
WHERE name LIKE '%Oleg%'

